/***
 * Packages Used:
 * ==================
 * -> Inertia-vue, Inertia-laravel 
 * -> Element UI Framework
 * 
 * Note: 
 * We have not used Blade Templating, Everything is Vuejs, Refer Inertia Laravel Docs
*/

import Inertia from 'inertia-vue'
import PortalVue from 'portal-vue'
import Vue from 'vue'
import VueMoment from 'vue-moment'

//ELement UI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'

Vue.config.productionTip = false

//Mixin for Inertia Links
Vue.mixin({ methods: { path: (...args) => window.route(...args).url() } })

Vue.use(Inertia)
Vue.use(PortalVue)
Vue.use(ElementUI, { locale })
Vue.use(VueMoment)

let app = document.getElementById('app')

new Vue({
  render: h => h(Inertia, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => import(`@/Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(app)
