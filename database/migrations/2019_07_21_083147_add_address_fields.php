<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->text('city')->after('address');
            $table->text('district')->after('city');
            $table->text('state')->after('district');
            $table->text('postcode')->after('state');
            $table->text('country')->after('postcode');
        });
    }

}
