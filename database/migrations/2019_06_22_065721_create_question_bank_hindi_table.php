<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionBankHindiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_bank_hindi', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->longText('question');
            $table->json('options');
            $table->string('question_id');
            $table->foreign('question_id')->references('id')->on('question_bank')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_bank_hindi');
    }
}
