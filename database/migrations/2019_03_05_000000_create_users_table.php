<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->string('first_name', 25);
            $table->string('last_name', 25);
            $table->string('username', 25)->unique();
            $table->string('email', 50)->unique();
            $table->string('phonenumber')->unique();
            $table->text('address');
            $table->string('password')->unique();
            $table->string('role')->default('student');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
