<?php

use App\User;
use App\Topics;
use App\Courses;
use App\Exams;
use App\QuestionBank;
use App\Subjects;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {

        // factory(User::class)->create([
        //     'first_name' => 'MJN',
        //     'last_name' => 'Dev',
        //     'email' => 'manju@example.com',
        //     'role' => 'student',
        //     'password' => 'test123'
        // ]);

        factory(User::class)->create([
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'admin@example.com',
            'role' => 'admin',
            'password' => 'admin'
        ]);

        // factory(Courses::class)->create([
        //     'name' => 'SSC',
        //     'wp_id' => 1,
        // ]);

        // factory(Courses::class)->create([
        //     'name' => 'Railways',
        //     'wp_id' => 2,
        // ]);

        // factory(Courses::class)->create([
        //     'name' => 'Others',
        //     'wp_id' => 3,
        // ]);

        //factory(User::class, 10)->create();

        
        //factory(Exams::class, 12)->create();

        //factory(Topics::class, 12)->create();

        //factory(QuestionBank::class,20)->create();


        // $organizations = factory(Organization::class, 100)
        //     ->create(['account_id' => $account->id]);

        // factory(Contact::class, 100)
        //     ->create(['account_id' => $account->id])
        //     ->each(function ($contact) use ($organizations) {
        //         $contact->update(['organization_id' => $organizations->random()->id]);
        //     });
    }
}
