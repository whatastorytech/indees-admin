<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Topics;
use Faker\Generator as Faker;

$factory->define(Topics::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'wp_id' => $faker->randomDigit,
        'subject_id' => $faker->numberBetween($min = 1, $max = 10),
    ];
});
