<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Exams;
use Faker\Generator as Faker;

$factory->define(Exams::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'course_id' => 1,
        'subjects_ids' => json_encode(['1','2','3']),
        'topics_ids' => json_encode(['1','2','3']),
        'questions_ids' => json_encode(['3','4','5']),
        'max_marks' => 100,
        'topics_ids' => json_encode(['1','2','3'])
    ];
});
