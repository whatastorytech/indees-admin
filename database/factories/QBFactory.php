<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\QuestionBank;
use Faker\Generator as Faker;

$factory->define(QuestionBank::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'course_id' => 1,
        'subject_id' => 2,
        'options' => json_encode(['1','2','3','4']),
        'answerkey' => $faker->numberBetween($min = 1, $max = 4),
        'topics_ids' => json_encode(['1','2','3'])
    ];
});
