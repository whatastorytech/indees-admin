<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Courses;
use Faker\Generator as Faker;

$factory->define(Courses::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'wp_id' => $faker->unique()->randomDigit,
    ];
});
