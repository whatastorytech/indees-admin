<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Subjects;
use Faker\Generator as Faker;

$factory->define(Subjects::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'wp_id' => $faker->randomDigit,
        'course_id' => $faker->randomDigit
    ];
});
