<?php
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ExamNotification;

// Auth
Route::get('login')->name('login')->uses('Auth\LoginController@showLoginForm');
Route::get('login/admin')->name('login.admin')->uses('Auth\LoginController@AdminLoginForm');
Route::get('auth/{token}')->name('wplogin')->uses('Auth\LoginController@LoginWithWP');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');
Route::post('logout')->name('logout')->uses('Auth\LoginController@logout');
Route::post('reset-password')->name('reset-password')->uses('Auth\LoginController@resetpassword');

// Dashboard
Route::get('/')->name('dashboard')->uses('DashboardController')->middleware('auth');

// Users
// Route::get('users')->name('users')->uses('UsersController@index')->middleware('remember', 'auth');
// Route::get('users/create')->name('users.create')->uses('UsersController@create')->middleware('auth');
// Route::post('users')->name('users.store')->uses('UsersController@store')->middleware('auth');
// Route::get('users/{user}/edit')->name('users.edit')->uses('UsersController@edit')->middleware('auth');
// Route::put('users/{user}')->name('users.update')->uses('UsersController@update')->middleware('auth');
// Route::delete('users/{user}')->name('users.destroy')->uses('UsersController@destroy')->middleware('auth');
// Route::put('users/{user}/restore')->name('users.restore')->uses('UsersController@restore')->middleware('auth');

Route::resource('users', 'UsersController');

Route::get('account/')->name('profile')->uses('ProfileController@show')->middleware('auth');
Route::get('account/update')->name('profile.update')->uses('ProfileController@show')->middleware('auth');
Route::get('account/purchases')->name('profile.purchases')->uses('ProfileController@show')->middleware('auth');
Route::post('account/avatar')->name('profile.avatar')->uses('ProfileController@updateAvatar')->middleware('auth');
Route::post('account/update')->name('profile.update')->uses('ProfileController@updateProfile')->middleware('auth');
Route::get('account/coupons')->name('profile.coupons')->uses('ProfileController@show')->middleware('auth');

Route::get('support/')->name('support')->uses('ProfileController@userSupport')->middleware('auth');
Route::get('support/technical')->name('support.technical')->uses('ProfileController@userSupport')->middleware('auth');
Route::get('support/contact')->name('support.contact')->uses('ProfileController@userSupport')->middleware('auth');

// students
Route::get('students')->name('students')->uses('StudentsController@index')->middleware('remember', 'auth');
Route::get('students/create')->name('students.create')->uses('StudentsController@create')->middleware('auth');
Route::post('students')->name('students.store')->uses('StudentsController@store')->middleware('auth');
Route::get('students/{student}/edit')->name('students.edit')->uses('StudentsController@edit')->middleware('auth');
Route::put('students/{student}')->name('students.update')->uses('StudentsController@update')->middleware('auth');
Route::delete('students/{student}')->name('students.destroy')->uses('StudentsController@destroy')->middleware('auth');
Route::put('students/{student}/restore')->name('students.restore')->uses('StudentsController@restore')->middleware('auth');

//Packages
Route::get('packages')->name('packages')->uses('PackagesController@index')->middleware('auth');
Route::get('packages/{package}/show')->name('packages.show')->uses('PackagesController@show')->middleware('auth');

//Purchases
Route::get('purchases')->name('purchases')->uses('PurchasesController@index')->middleware('auth');


// Reports
Route::get('reports')->name('reports')->uses('ReportsController')->middleware('auth');
Route::get('leaderboard')->name('leaderboard')->uses('LeaderboardController')->middleware('auth');

// results
Route::post('result')->name('result.store')->uses('ResultController@store')->middleware('auth'); 
Route::get('result/{result}/')->name('result.show')->uses('ResultController@show')->middleware('auth');

//Exams
Route::get('exams')->name('exams')->uses('ExamsController@index')->middleware('remember', 'auth');
Route::get('exams/create')->name('exams.create')->uses('ExamsController@create')->middleware('auth');
Route::post('exams')->name('exams.store')->uses('ExamsController@store')->middleware('auth'); 
Route::get('exams/{exam}/edit')->name('exams.edit')->uses('ExamsController@edit')->middleware('auth');
Route::put('exams/{exam}')->name('exams.update')->uses('ExamsController@update')->middleware('auth');
Route::put('exams/{exam}/publish')->name('exams.publish')->uses('ExamsController@publish')->middleware('auth');
Route::get('exams/{exam}/show')->name('exams.show')->uses('ExamsController@show')->middleware('auth');
Route::get('exam/{id}')->name('exam.take')->uses('ExamsController@examui')->middleware('remember', 'auth');
Route::delete('exam/{id}')->name('exam.destroy')->uses('ExamsController@destroy')->middleware('auth');

//Question Bank
Route::get('question-bank')->name('question-bank')->uses('QBController@index')->middleware('remember', 'auth');
Route::get('question-bank/create')->name('question-bank.create')->uses('QBController@create')->middleware('auth');
Route::post('question-bank')->name('question-bank.store')->uses('QBController@store')->middleware('auth'); 
Route::get('question-bank/{question}/edit')->name('question-bank.edit')->uses('QBController@edit')->middleware('auth');
Route::put('question-bank/{question}')->name('question-bank.update')->uses('QBController@update')->middleware('auth');
Route::get('question-bank/{question}/show')->name('question-bank.show')->uses('QBController@show')->middleware('auth');
Route::get('question-bank/{question}/solution')->name('question-bank.solution')->uses('QBController@solution')->middleware('auth');
Route::delete('question-bank/{question}')->name('question-bank.destroy')->uses('QBController@destroy')->middleware('auth');

//Search
Route::get('search/students')->name('search.students')->uses('SearchDB@searchStudents')->middleware('remember', 'auth');

//Get Questions
Route::post('questions')->name('questions')->uses('QBController@getQuestions')->middleware('remember', 'auth');


//Solutions
Route::post('solution')->name('solution.store')->uses('SolutionsController@store')->middleware('auth'); 

Route::get('/test-mail', function (){
    $exam = App\Exams::first();
    Auth::user()->notify(new ExamNotification($exam));
    return 'Sent';
});

Route::get('/testx', function (){
    $grouped = App\Result::where('exam_id','9a53f367-5bf9-47d1-854a-15c924e59f0e')->get()->sortByDesc('score')->groupBy('score')->transform(function($item, $k) {
        return $item->groupBy('username');
    });
    dd($grouped);
});