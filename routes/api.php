<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API Routes without custom Auth
Route::resource('subjects', 'Api\SubjectsController');
Route::resource('topics', 'Api\TopicsController');
Route::resource('packages', 'Api\PackagesController');
Route::resource('users', 'Api\UsersController');
Route::resource('purchases', 'Api\PurchasesController');


Route::get('login', 'Api\LoginController@login');