# Indees

Indees Speed Test is a Compitative Examination Portal 

* [Installation](#markdown-header-installation)
* [Changelog](#markdown-header-changelog)
* [Credentials](#markdown-header-credentials)

 
# Credentials

```sh

 Wordpress Admin : https://wordpress-180549-1033492.cloudwaysapps.com/wp-admin
 username : ind_admin
 password : IndeesAdmin!@#

 Portal : http://phplaravel-180549-1033515.cloudwaysapps.com/login
 username : admin@example.com
 password : admin

 Wordpress Student : https://wordpress-180549-1033492.cloudwaysapps.com/

 Test Card Details for Razorpay Checkout

 4111 1111 1111 1111
 Name : Any Name
 Expiry : 12/22
 CVV : 111

```

# Changelog

### October 30, 2019

* Fixed Razor Pay Payment Integration
* Fixed Login Auth and Redirection to Portal
* Fixed Posting Users data to Portal

### October 27, 2019

* UI Improvemnets
    * Navigation Change
    * Onboarding Workflow on Student purchase
    * Change in styles of Exams and Reports
    * Dashboard UI Change
    * Leader Boards 
* Application
    * API Authentication
    * Custom Login Auth
    * Leaderboards Improvement 
* Wordpress
    * Custom login
    * Custom Password Reset
    * Redirection to Portal with Custom auth from WP

### September 25, 2019

* Email Notifications
* Speed tests
* Scheduling exam for all students
* Scheduling speed test for all students
* Scheduling specific mock exam or test for a single student
* Leader Board
* Wordpress
    * Purchase Emails
    * Order Notifications
    * Razorpay Integration (Done, live remaining)
    * Student WP data integration to Portal (Done, live remaining)

### August 30, 2019

* Fixed Errors in Production
* Changed Codes with Lodash
* Added Solution Indicator in Question Bank
* Added Time Durtaion in Exams
* Added Timer to Exam Taking Feature
* Added Filters to Add Question Popup in Exam
* Fixed UI of Add Question Popup in Exam
* Optamized List of Questions only to Show related to Exam Subjects in Add Question Popup 
* Fixed Filters in Question Bank, Exams Listings

### August 26, 2019

* Reports with List of Exams and Reports
* Exam Result with Subject Wise and Topic Wise Breakdown

### August 14, 2019 

**Wordpress**

* Razorpay Payments Integration
* Purchases Email to Students


**Student Dashboard**

* Exams Listing sorted by packages
* Exam Taking Feature
* Question Bookmark Feature
* Result Generation
* Result with Comparision with Previous Attempts
* Interactive Chart/Graph based results


## August 5, 2019

**Wordpress**

* Package Purchases
* Student Login/Signup
* Data Integration of Student to Student Portal

**Student Dashboard**

* Student Portal Login
* Purchases History
* Packages Listing
* Exams Listing

## July 19, 2019

* Fixed Hindi Only Questions Internal Server Error


## July 17, 2019

* Fixed Questions showing incorrect answers in Solutions


## July 16, 2019

* Fixed Video Upload issues


## July 11, 2019

**Wordpress**

* Custom Post type Packages
* Custom Taxonomies ( Subjects, Topics)
* Data transfer on creation,edit,update to App API
  
**Admin Dashoard**

* Recreation of Database Structure
* Modified Database Migrations
* Removed Courses and Respective Pages,Codes.
* Added Packages
* API Creation for Data Integration to WP
* Data Connectivity between WP and App (Packages,Subjects,Topics)
* Fixed Copy paste/Drag and Drop images within Question Editor


## July 3, 2019

* Modified Student Role Views
* Student Portal Dashboard


## June 26, 2019

* Added Filters to Exams/Question Bank
* Fixed Image upload to Questions
  
## June 22, 2019

* Bilangual Feature
* Video External Links option added for video solution
* Added options for choosing questions to add directly within exam details tab
* Add Subjects/Topics without going to seperate pages

## June 19, 2019

* Fixed Courses list not dynamically avilable on all exams

## June 16, 2019

* Question Bank
* Solutions Add/Edit/Update
* Video Solutions Upload

##Update 1

**Wordpress**

* Landing Page
* Mega Menu
* User Registration
* Payments Integration (Paypal)
* User Login
* Woocommerce Store
* Package Popup with Details (Needs improvements based on data)

**Admin Dashboard**

* Admin Login
* Widgets Panel with Details (Stats)
* Courses Addition/Update
* Subjects Addition/Update
* Topics Addition/Update
* Data integration from WP
* Exams Creation/Edit/Update
* Question Creation/Edit/Update
* Exams with Associated Questions List
* Question Details with Solutions
* Solutions Creation/Edit/Update
* Video Solutions with Third party links support (Need to add Link Support)
* Canvas For Solutions Creation ( Needs Improvements)
* Students List with Details associated
* Questions with Options for Multiple Answers or dynamic options
* Math Equations Integrations
* Question Addition/Deletion from Exam 
* Filters for exam and question bank
* Bi-langual Integrations

 
# Installation

Clone the repo locally:

```sh
git clone https://manjumjn@bitbucket.org/whatastory/indees-admin.git

cd indees-admin
```

Install PHP dependencies:

```sh
composer install
```

Install NPM dependencies:

```sh
npm install
```

Build assets:

```sh
npm run dev
```

Setup configuration:

```sh
cp .env.example .env
```

Generate application key:

```sh
php artisan key:generate
```

Create an MySQL database and Configure in .env file and Run database migrations:

```sh
php artisan migrate
```

Run database seeder (Optional):

```sh
php artisan db:seed
```

You're ready to go! Visit Application in your browser, and login with credentials given in Seeder

----
# Depreceated System
----
## April 20, 2019

### Authentication

- **User Register**

    ![](readme/11.jpg)

- **User Auth Key codes**

    ![](readme/12.jpg)

    ![](readme/13.jpg)

- **Data Access without Token**

    ![](readme/14.jpg)

- **Data Access with Token**

    ![](readme/15.jpg)

## Bug Fixes & Improvements

- Updated REST API routes.
- Add Authentication for data access.
- Add user access based on roles for Data access.

# April 8, 2019

## Admin Frontend

- Dashboard
    ![Admin Dashboard Homepage](readme/1.png)

- Courses Page

    ![](readme/2.png)

- Add/Remove Subjects

    ![Dashboard Frontend](readme/3.png)
    ![Saved Info Message](readme/4.png)
    ![Confirm Message Popup for Delete](readme/5.png)


## **Backend Server API Listings**

- Server Request/Responses

    ![Users List](readme/6.jpg)

    ![Courses List](readme/7.jpg)

    ![Add Course API Request](readme/8.jpg)

    ![Updated course List](readme/9.jpg)

    ![Subjects Request](readme/10.jpg)


## Bug Fixes & Improvements

- Dynamic Subjects Listing Real-time update on Add/Remove
- Added Delete Buttons to Subjects/Topics
- Added Message Info Boxes on Successful/Not for Save and Delete Actions
