<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    use UsesUuid;

    protected $fillable = ['name','wp_id','slug','price','img','url','exam_ids'];

    public function exam()
    {
        return $this->hasMany('App\Exams','id');
    }
}
