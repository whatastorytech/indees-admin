<?php

namespace App\Notifications;
use App\Exams;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ExamNotification extends Notification
{
    use Queueable;

    public $exam;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Exams $exam)
    {
        $this->exam = $exam;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->greeting('Hello!')
                ->line('New Exam has been Published. Practice and Score Better!!')
                ->action('Login Now', 'https://uppariksha.com')
                ->line('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'name' => 'New Exam Published!',
            'description' => $this->exam->name . ' for total ' . $this->exam->max_marks . ' marks',
            'exam_id' => $this->exam->id
        ];
    }
}
