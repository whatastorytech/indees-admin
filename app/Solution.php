<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{
    use UsesUuid;
    
    protected $fillable = ['question_id','video_url','explaination'];

    public function question()
    {
        return $this->belongsTo('App\QuestionBank','question_id');
    }

}
