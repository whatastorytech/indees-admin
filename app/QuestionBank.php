<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{

    use UsesUuid;

    protected $table = 'question_bank';

    protected $fillable = ['question','options','answerkey','subject_id','topics_ids'];

    protected $casts = [
        'options' => 'array',
        'topics_ids' => 'array',
        'answerkey' => 'array'
    ];

    // public function scopeFilter($query, array $filters)
    // {
    //     $query->when($filters['search'] ?? null, function ($query, $search) {
    //         $query->where(function ($query) use ($search) {
    //             $query->where('question', 'like', '%'.$search.'%');
    //         });
    //     })->when($filters['trashed'] ?? null, function ($query, $trashed) {
    //         if ($trashed === 'with') {
    //             $query->withTrashed();
    //         } elseif ($trashed === 'only') {
    //             $query->onlyTrashed();
    //         }
    //     })->when($filters['from'] ?? null, function ($query, $from) {
    //         $query->where(function ($query) use ($from) {
    //             $query->where('created_at', '>=',date($from));
    //         });
    //     })->when($filters['to'] ?? null, function ($query, $to) {
    //         $query->where(function ($query) use ($to) {
    //             $query->where('created_at', '<=', date($to));
    //         });
    //     });
    // }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['subjects'] ?? null, function ($query, $data) {
            $query->where('subject_id', 'like', '%'.$data.'%');
        })->when($filters['topics'] ?? null, function ($query, $data) {
            $topic_array = explode(',', htmlspecialchars_decode($data));
            foreach($topic_array as $key => $value){
                $topic_array[$key] = (String) $value;
            }
            $topic_array = '"' . implode('","',$topic_array) .'"';
            $query->whereRaw("JSON_CONTAINS(topics_ids, '[{$topic_array}]')");
        })->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('question', 'like', '%'.$search.'%');
        });
    }

    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = json_encode($value);
    }

    public function getOptionsAttribute($details)
    {
        return json_decode($details, true);
    }

    public function setTopicsIdsAttribute($value)
    {
        $this->attributes['topics_ids'] = json_encode($value);
    }

    public function getSubjectsIdsAttribute($details)
    {
        return json_decode($details, true);
    }

    public function setAnswerkeyAttribute($value)
    {
        $this->attributes['answerkey'] = json_encode($value);
    }

    public function getAnswerkeyAttribute($details)
    {
        return json_decode($details, true);
    }

    public function solution()
    {
        return $this->hasOne('App\Solution','question_id');
    }
}
