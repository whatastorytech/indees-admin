<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBankHindi extends Model
{
    use UsesUuid;

    protected $table = 'question_bank_hindi';

    protected $fillable = ['question_id','question','options'];

    protected $casts = [
        'options' => 'array'
    ];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('question', 'like', '%'.$search.'%');
            });
        })->when($filters['trashed'] ?? null, function ($query, $trashed) {
            if ($trashed === 'with') {
                $query->withTrashed();
            } elseif ($trashed === 'only') {
                $query->onlyTrashed();
            }
        });
    }

    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = json_encode($value);
    }

    public function getOptionsAttribute($details)
    {
        return json_decode($details, true);
    }

}
