<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    use UsesUuid;

    public $timestamps = false;

    protected $fillable = ['name','wp_id','slug'];

}
