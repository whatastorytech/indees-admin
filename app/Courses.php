<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subjects;

class Courses extends Model
{
    protected $fillable = ['name','wp_id'];

    public function getRouteKeyName()
    {
        return 'name';
    } 
}
