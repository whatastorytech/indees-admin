<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exams extends Model
{
    use UsesUuid;

    protected $casts = [
        'subjects_ids' => 'array',
        'questions_ids' => 'array',
        'topics_ids' => 'array',
        'students_id' => 'array'
    ];

    protected $fillable = ['name','package_id','max_marks','time','subjects_ids','topics_ids','questions_ids','status','expiry','start','students_id','is_speed_test','is_students_specific'];

    public function setSubjectsIdsAttribute($value)
    {
        $this->attributes['subjects_ids'] = json_encode($value);
    }

    public function setQuestionsIdsAttribute($value)
    {
        $this->attributes['questions_ids'] = json_encode($value);
    }

    public function setTopicsIdsAttribute($value)
    {
        $this->attributes['topics_ids'] = json_encode($value);
    }

    public function setStudentsIdAttribute($value)
    {
        $this->attributes['students_id'] = json_encode($value);
    }

    public function getStudentsIdAttribute($details)
    {
        return json_decode($details, true);
    }

    public function getSubjectsIdsAttribute($details)
    {
        return json_decode($details, true);
    }
    
    public function getQuestionsIdsAttribute($details)
    {
        return json_decode($details, true);
    }

    public function getTopicsIdsAttribute($details)
    {
        return json_decode($details, true);
    }

    public function scopeFilter($query, array $filters){
        $query->when($filters['subjects'] ?? null, function ($query, $data) {
            $subjects_ids = htmlspecialchars_decode($data);
            $query->whereRaw("JSON_CONTAINS(subjects_ids, '[{$subjects_ids}]')");
        })->when($filters['topics'] ?? null, function ($query, $data) {
            $topics_ids = htmlspecialchars_decode($data);
            $query->whereRaw("JSON_CONTAINS(topics_ids, '[{$topics_ids}]')");
        })->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('name', 'like', '%'.$search.'%');
        });
    }

    public function questions()
    {
        return $this->hasMany(QuestionBank::class);
    }

    public function package()
    {
        return $this->belongsTo('App\Packages','package_id');
    }

    public function results()
    {
        return $this->hasMany('App\Result','exam_id','id');
    }
}
