<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function scopeOrderByName($query)
    {
        $query->orderBy('last_name')->orderBy('first_name');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['package'] ?? null, function ($query, $data) {
            //$query->whereHas('purchases',function($query){
                //dd($query->purchases);
                $query->purchases->where('package_id', 'like', '%'.$data.'%');
            //});
        })->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%')
                    ->orWhere('phonenumber', 'like', '%'.$search.'%')
                    ->orWhere('email', 'like', '%'.$search.'%');
        });
    }

}
