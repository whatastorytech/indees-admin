<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Notifiable, SoftDeletes, Authenticatable, Authorizable, UsesUuid;
    
    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function scopeOrderByName($query)
    {
        $query->orderBy('last_name')->orderBy('first_name');
    }


    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['package'] ?? null, function ($query, $data) {
            $query->whereHas('purchases',function($query) use($data){
                $query->where('package_id', 'like', '%'.$data.'%');
            });
        })->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%')
                    ->orWhere('phonenumber', 'like', '%'.$search.'%')
                    ->orWhere('email', 'like', '%'.$search.'%');
        });
    }

    public function session()
    {
        return $this->hasOne(App\UserSession::class);
    }

    public function results()
    {
        return $this->hasMany('App\Result', 'username', 'username');
    }

    public function purchases()
    {
        return $this->hasMany('App\Purchases','username','username');
    }
}
