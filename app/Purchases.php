<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    use UsesUuid;
    
    protected $fillable = ['order_id','package_id','username'];
}
