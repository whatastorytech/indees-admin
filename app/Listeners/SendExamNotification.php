<?php

namespace App\Listeners;

use Notification;
use App\Events\ExamCreated;
use Illuminate\Support\Facades\Auth;
use App\Notifications\ExamNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendExamNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExamCreated  $event
     * @return void
     */
    public function handle(ExamCreated $event)
    {
        $exam = $event->exam;
        $usernames = \App\Purchases::where('package_id','=',$exam->package->wp_id)->pluck('username');
        $users = \App\User::whereIn('username', $usernames)->get();
        //dd($usernames,$users);
        Notification::send($users, new ExamNotification($exam));
    }
}
