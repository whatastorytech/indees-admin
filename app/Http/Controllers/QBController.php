<?php

namespace App\Http\Controllers;


use Inertia\Inertia;
use App\QuestionBank;
use App\QuestionBankHindi;
use App\Solution;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Request;
use App\Subjects;
use App\Exams;

class QBController extends Controller
{

    public function getQuestions(\Illuminate\Http\Request $request)
    {

        $exam = Exams::find($request->exam_id);
        // dd($exam);
        $newquestions = [];
        $subjects_ids = Subjects::whereIn('wp_id',$exam->subjects_ids)->pluck('id')->toArray();
    
        $newquestions = QuestionBank::whereNotIn('id',$request->ids)->whereIn('subject_id',$subjects_ids)
        ->orderBy('created_at', 'DESC')
        ->limit(100)->get()
        ->transform(function ($q) {
            return [
                'id' => $q->id,
                'question' => $q->question,
                'subject_id' => $q->subject_id,
                'topics_ids' => $q->topics_ids
            ];
        })->toArray();
        
        return response()->json(['questions' => $newquestions]);

    }

    public function index(){
        $questions =  QuestionBank::filter(Request::only('search', 'subjects','topics','from','to'))
        ->with('solution')
        ->orderBy('created_at', 'DESC')
        ->paginate(100)
        ->transform(function ($q) {
            return [
                'id' => $q->id,
                'question' => $q->question,
                'options' => $q->options,
                'answer' => $q->answerkey,
                'subject_id' => $q->subject_id,
                'topics_ids' => $q->topics_ids,
                'solution' => $q->solution == null ? false : true
            ];
        });
        //dd($questions);
        return Inertia::render('QB/Index', [
            'filters' => Request::all('search', 'subjects','topics','from','to'),
            'questions' => $questions,
            'subjects' => \App\Subjects::all(),
            'topics' => \App\Topics::all()
        ]);
    }

    public function create()
    {
        return Inertia::render('QB/Create',[
            'subjects' => \App\Subjects::all(),
            'topics' => \App\Topics::all(),
        ]);
    }

    public function store(\Illuminate\Http\Request $request)
    {
        $newq = QuestionBank::create([
            'question' => $request->question,
            'options' => $request->options,
            'answerkey' => $request->answer,
            'subject_id' => $request->subject_id,
            'topics_ids' => $request->topics_id,
        ]);

        QuestionBankHindi::create([
            'question' => $request->question_hindi,
            'options' => $request->options_hindi,
            'question_id' => $newq->id
        ]);

        return Redirect::route('question-bank')->with('success', 'Question Added.');
    }
    
    public function update(QuestionBank $question,\Illuminate\Http\Request $request)
    {
        //dd($request->question_hindi);
        $hindiquestion = QuestionBankHindi::where('question_id',$question->id)->first();

        $question->update([
            'question' => $request->question,
            'options' => $request->options,
            'answerkey' => $request->answer,
            'subject_id' => $request->subject_id,
            'topics_ids' => $request->topics_id,
        ]);

        $hindiquestion->update([
            'question' => $request->question_hindi,
            'options' => $request->options_hindi,
        ]);

        return Redirect::route('question-bank.show', $question)->with('success', 'Question updated.');
    }

    public function show(QuestionBank $question)
    {
        //dd($question->solution);

        $qdatahindi = QuestionBankHindi::where('question_id',$question->id)->first()->toArray();

        $qdata = [
            'id' => $question->id,
            'question' => $question->question,
            'question_hindi' => $qdatahindi['question'],
            'options' => $question->options,
            'options_hindi' => $qdatahindi['options'],
            'answerkey' => $question->answerkey
        ];
        $qname = $question->question;

        return Inertia::render('QB/Show',[
            'qdata' => $qdata,
            'solution' => Solution::where('question_id',$question->id)->get()
            ->transform(function ($sol) {
                return [
                    'id' => $sol->id,
                    'url' => $sol->video_url,
                    'explaination' => $sol->explaination
                ];
            }),
        ]);
    }

    public function solution(QuestionBank $question)
    {
        $qdatahindi = QuestionBankHindi::where('question_id',$question->id)->first()->toArray();

        $qdata = [
            'id' => $question->id,
            'question' => $question->question,
            'question_hindi' => $qdatahindi['question'],
            'options' => $question->options,
            'options_hindi' => $qdatahindi['options'],
            'answerkey' => $question->answerkey
        ];
        $qname = $question->question;

        return Inertia::render('QB/Solution',[
            'qdata' => $qdata ,
            'solution' => Solution::where('question_id',$question->id)->get()
            ->transform(function ($sol) {
                return [
                    'id' => $sol->id,
                    'url' => $sol->video_url,
                    'explaination' => $sol->explaination
                ];
            }),
        ]);
    }

    public function edit(QuestionBank $question)
    {
        $qdatahindi = QuestionBankHindi::where('question_id',$question->id)->first()->toArray();

        $qdata = [
            'id' => $question->id,
            'question' => $question->question,
            'question_hindi' => $qdatahindi['question'],
            'options' => $question->options,
            'options_hindi' => $qdatahindi['options'],
            'answerkey' => $question->answerkey,
            'subject_id' => $question->subject_id,
            'topics_ids' => $question->topics_ids,
        ];

        return Inertia::render('QB/Edit', [
            'qdata' => $qdata,
            'subjects' => \App\Subjects::all(),
            'topics' => \App\Topics::all(),
        ]);
    }


    public function destroy($id)
    {
        $question = QuestionBank::find($id);

        $question->delete();

        return Redirect::route('question-bank')->with('success', 'Question Deleted.');
    }
}
