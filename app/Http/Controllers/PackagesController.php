<?php

namespace App\Http\Controllers;

use App\Topics;
use App\Packages;
use App\Subjects;
use App\Exams;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class PackagesController extends Controller
{

    public function index(){
        return Inertia::render('Packages/Index', [
            'packages' => Packages::all()
        ]);
    }

    public function show(Packages $package){

        $exams = Exams::all()->where('package_id', '==', $package->id);

        return Inertia::render('Packages/Show', [
            'title' => $package->name,
            'exams' => $exams,
            'subjects' => Subjects::all(),
            'topics' => Topics::all(),
        ]);
    }
}
