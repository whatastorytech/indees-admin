<?php

namespace App\Http\Controllers;


use Inertia\Inertia;
use App\Exams;
use App\Purchases;
use App\Packages;
use App\Result;
use App\Subjects;
use App\Topics;
use App\QuestionBank;
use App\QuestionBankHindi;
use App\Events\ExamCreated;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ExamsController extends Controller
{

    public function index(){

        $role = Auth::user()->role;
        $username = Auth::user()->username;
        $userid = Auth::user()->id;

        if($role === "admin"){

            $packages = Packages::all()->transform(function ($data) {
                return [
                    'id' => $data->wp_id,
                    'name' => $data->name
                ];
            });

            $exams = Exams::filter(\Request::only('search', 'subjects','topics'))
                ->orderBy('status', 'DESC')
                ->orderBy('updated_at', 'DESC')
                ->paginate(50)
                ->transform(function ($exam) {
                    return [
                        'id' => $exam->id,
                        'name' => $exam->name,
                        'marks' => $exam->max_marks,
                        'time' => $exam->time,
                        'subjects_ids' => $exam->subjects_ids,
                        'topics_ids' => $exam->topics_ids,
                        'created' => $exam->created_at->format('M, Y'),
                        'package_id' => $exam->package_id,
                        'status' => $exam->status,
                        'type' => $exam->is_speed_test,
                        'start' => Carbon::parse($exam->start)->format('d, M, Y'),
                        'expiry' => Carbon::now('Asia/Kolkata')->gt(Carbon::parse($exam->expiry))
                    ];
                });

            return Inertia::render('Exams/Index', [
                'filters' => \Request::all('search', 'subjects','topics'),
                'packages' => $packages,
                'exams' => $exams,
                'subjects' => Subjects::all(),
                'topics' => Topics::all(),
            ]);
        } 
        else{
            $package_wp_ids = Purchases::where('username','=',$username)->pluck('package_id')->toArray();

            $package_ids = Packages::whereIn('wp_id', $package_wp_ids)->pluck('id')->toArray();
            
            $packages = Packages::whereIn('wp_id', $package_wp_ids)->get()->toArray();

            $examstaken = Result::where('username','=',$username)->get()->toArray();

            $now = Carbon::now('Asia/Kolkata')->toDateTimeString();

            $week = Carbon::now('Asia/Kolkata')->add(1, 'week')->toDateTimeString();

            $qry = Exams::whereIn('package_id',$package_ids)->where('status', 'live')->where(function ($q) use ($now) {
                        $q->where('start', '<=', $now )->where('expiry', '>=', $now);
                    })->orderBy('start', 'DESC');

            $exams = $qry->get()->transform(function ($exam) {
                return [
                    'id' => $exam->id,
                    'name' => $exam->name,
                    'marks' => $exam->max_marks,
                    'time' => $exam->time,
                    'subjects_ids' => $exam->subjects_ids,
                    'created' => $exam->created_at->format('M, Y'),
                    'available' => Carbon::now('Asia/Kolkata')->gt(Carbon::parse($exam->expiry)),
                    'report' => Carbon::parse($exam->expiry)->addDay()->format('M d, Y'),
                    'package_id' => $exam->package_id
                ];
            });

            //dd($exams);

            $specific_exams = Exams::where('students_id', 'LIKE', "%{$userid}%")->where('status', 'live')
                ->where(function ($q) use ($now) {
                    $q->whereDate('start', '<=', $now )->WhereDate('expiry', '>=', $now);
                })->orderBy('start', 'DESC')->get()
                ->transform(function ($exam) {
                    return [
                        'id' => $exam->id,
                        'name' => $exam->name,
                        'marks' => $exam->max_marks,
                        'time' => $exam->time,
                        'subjects_ids' => $exam->subjects_ids,
                        'created' => $exam->created_at->format('M, Y'),
                        'package_id' => $exam->package_id
                    ];
                });


            $nqry = Exams::whereIn('package_id',$package_ids)->where('status', 'live')
                ->where(function ($q) use ($now,$week) {
                    $q->whereDate('start', '>=', $now )->WhereDate('start', '<=', $week);
                })->orderBy('start', 'DESC');    

            $uexams = $nqry->get()->transform(function ($exam) {
                    return [
                        'id' => $exam->id,
                        'name' => $exam->name,
                        'marks' => $exam->max_marks,
                        'time' => $exam->time,
                        'subjects_ids' => $exam->subjects_ids,
                        'created' => $exam->created_at->format('M, Y'),
                        'package_id' => $exam->package_id
                    ];
                });

            $pqry = Exams::whereIn('package_id',$package_ids)->where('status', 'live')->where('expiry','<',$now)->orderBy('start', 'DESC');

            $prev_exams = $pqry->get()->transform(function ($exam) {
                    return [
                        'id' => $exam->id,
                        'name' => $exam->name,
                        'marks' => $exam->max_marks,
                        'time' => $exam->time,
                        'subjects_ids' => $exam->subjects_ids,
                        'created' => $exam->created_at->format('M, Y'),
                        'package_id' => $exam->package_id
                    ];
                });

            $subjects = Subjects::all()->transform(function ($data) {
                return [
                    'id' => $data->wp_id,
                    'name' => $data->name
                ];
            });

            return Inertia::render('Exams/Student/Index',[
                'exams' => $exams,
                'upcoming' => $uexams,
                'subjects'=>$subjects,
                'exams_taken'=>$examstaken,
                'specific_exams' => $specific_exams,
                'prev_exams' => $prev_exams
            ]);
        }
    }

    public function show(Exams $exam)
    {
        $questions_ids =  $exam->questions_ids;
        $questions = [];
        $newquestions = [];
        $subjects_ids = Subjects::whereIn('wp_id',$exam->subjects_ids)->pluck('id')->toArray();
    
        if(is_iterable($questions_ids)){
            $questions = QuestionBank::whereIn('id',$exam->questions_ids)
            ->with('solution')
            ->orderBy('created_at', 'DESC')
            ->paginate(50)
            ->transform(function ($q) {
                return [
                    'id' => $q->id,
                    'question' => $q->question,
                    'options' => $q->options,
                    'subject_id' => $q->subject_id,
                    'topics_ids' => $q->topics_ids,
                    'answer' => $q->answerkey,
                    'solution' => $q->solution == null ? false : true
                ];
            });
        }

        if(sizeof($questions_ids)){
            $newquestions = QuestionBank::whereNotIn('id',$questions_ids)->whereIn('subject_id',$subjects_ids)
            ->with('solution')
            ->orderBy('created_at', 'DESC')
            ->limit(150)->get()
            ->transform(function ($q) {
                return [
                    'id' => $q->id,
                    'question' => $q->question,
                    'subject_id' => $q->subject_id,
                    'topics_ids' => $q->topics_ids,
                    'solution' => $q->solution == null ? false : true
                ];
            })->toArray();
        } else{
            $newquestions = QuestionBank::whereIn('subject_id',$subjects_ids)
            ->with('solution')
            ->orderBy('created_at', 'DESC')
            ->limit(150)->get()
            ->transform(function ($q) {
                return [
                    'id' => $q->id,
                    'question' => $q->question,
                    'options' => $q->options,
                    'answer' => $q->answerkey,
                    'subject_id' => $q->subject_id,
                    'topics_ids' => $q->topics_ids,
                    'solution' => $q->solution == null ? false : true
                ];
            });
        }
        
        return Inertia::render('Exams/Show',[
            'exam' => [
                'id' => $exam->id,
                'name' => $exam->name,
                'marks' => $exam->max_marks,
                'time' => $exam->time,
                'package_id' => $exam->package_id,
                'subjects_ids' => $exam->subjects_ids,
                'topics_ids' => $exam->topics_ids,
                'questions_ids' => $exam->questions_ids,
                'status' => $exam->status,
                'expiry' => $exam->expiry,
                'start' => $exam->start
            ],
            'questions' => $questions,
            'newquestions' => $newquestions,
            'subjects' => Subjects::whereIn('wp_id',$exam->subjects_ids)->get(),
            'topics' => Topics::whereIn('wp_id',$exam->topics_ids)->get()
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Exams/Create',[
            'packages' => Packages::all(),
            'subjects' => Subjects::all(),
            'topics' => Topics::all(),
        ]);
    }

    public function store(Request $request)
    {
        //dd(Carbon::parse($request->expiry)->toDateTimeString());
        $exam = Exams::create([
            'name' => $request->name,
            'max_marks' => $request->marks,
            'package_id' => $request->package_id,
            'time' => $request->time,
            'subjects_ids' => $request->subjects_ids,
            'topics_ids' => $request->topics_ids,
            'questions_ids' => $request->questions_ids,
            'status' => $request->status,
            'expiry' => Carbon::parse($request->expiry)->timezone('Asia/Kolkata')->toDateTimeString(),
            'is_speed_test' => $request->speedtest,
            'is_students_specific' => $request->specific,
            'students_id' => $request->students,
            'start' => Carbon::parse($request->start)->timezone('Asia/Kolkata')->toDateTimeString()
        ]);
        return Redirect::route('exams.show',$exam->id)->with('success', 'Exam created. Add Question to this exam');
    }

    public function edit(Exams $exam)
    {
        return Inertia::render('Exams/Edit', [
            'exam' => [
                'id' => $exam->id,
                'name' => $exam->name,
                'marks' => $exam->max_marks,
                'time' => $exam->time,
                'package_id' => $exam->package_id,
                'subjects_ids' => $exam->subjects_ids,
                'topics_ids' => $exam->topics_ids,
                'status' => $exam->status,
                'expiry' => $exam->expiry,
                'is_speed_test' => $exam->is_speed_test,
                'specific' => $exam->is_students_specific,
                'students_id' => $exam->students_id,
                'start' => $exam->start
            ],
            'subjects' => Subjects::all(),
            'topics' => Topics::all()
        ]);
    }

    public function update(Exams $exam,Request $request)
    {
        $question_ids =  $exam->questions_ids;
        $qupdate = false;
        $isRemoval = false;

        if(!$request->name){
            $qupdate = true;
            if(sizeof($request->removalQids) > 0 )
                $isRemoval = true;
            else 
                $isRemoval = false;
        } else{
            $qupdate = false;
        }

        if($qupdate){
            if($isRemoval){
                $updatedQids = array_values(array_diff($question_ids, $request->removalQids));
                $exam->update([ 'questions_ids' => $updatedQids ]);
                return Redirect::route('exams.show', $exam)->with('success', 'Selected Questions Removed.');

            } else{
                if(sizeof($question_ids) > 0){
                    $newquestionIds = array_merge($question_ids,$request->selectedQuestionsIds);
                    $exam->update([
                        'questions_ids' => $newquestionIds
                    ]);
                } else{
                    $exam->update([
                        'questions_ids' =>$request->selectedQuestionsIds,
                    ]);
                }
                return Redirect::route('exams.show', $exam)->with('success', 'Questions Updated.');
            }
        } else{ 
            $exam->update([
                'name' => $request->name,
                'time' => $request->time,
                'max_marks' => $request->marks,
                'package_id' => $request->package_id,
                'subjects_ids' => $request->subjects_ids,
                'topics_ids' => $request->topics_ids,
                'status' => $request->status,
                'expiry' => Carbon::parse($request->expiry)->timezone('Asia/Kolkata')->toDateTimeString(),
                'is_speed_test' => $request->speedtest,
                'is_students_specific' => $request->specific,
                'students_id' => $request->students,
                'start' => Carbon::parse($request->start)->timezone('Asia/Kolkata')->toDateTimeString()
            ]);

            if($request->status == 'live')
                //event(new ExamCreated($exam));

            return Redirect::route('exams.show', $exam)->with('success', 'Exam updated.');
        }
        
    }

    public function examui($id){

        if(Auth::user()->district == null || Auth::user()->state == null || Auth::user()->city == null){
            return Redirect::route('exams')->with('profile', true);
        }

        $exam = Exams::find($id);
        $questions = QuestionBank::whereIn('id',$exam->questions_ids)->get()->toArray();

        $questions_hindi = QuestionBankHindi::whereIn('question_id',$exam->questions_ids)->get()->toArray();

        $now = Carbon::now();
        $start_date = Carbon::parse($exam->start);
        $end_date = Carbon::parse($exam->expiry);

        $attempts = Result::where('username',Auth::user()->username)->where('exam_id',$id)->first();

        //dd($attempts);
        if($attempts != null){
            $attempted = true ;
        } else{
            $attempted = false ;
        }
        
        //dd($attempted);

        if($start_date->gt($now)){
            return Redirect::route('exams')->with('success', 'Exam is yet to start.');
        }

        return Inertia::render('Exams/Student/Take', [
            'username' => Auth::user()->username,
            'name' => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            'avatar' => url('').'/avatars/'.Auth::user()->avatar,
            'exam' => $exam,
            'subjects' => Subjects::all(),
            'topics' => Topics::all(),
            'questions' => $questions,
            'questions_hindi' => $questions_hindi,
            'attempted' => $attempted
        ]);
    }

    public function publish(Exams $exam)
    {
        $exam->update([ 'status' => 'live' ]);
        //event(new ExamCreated($exam));
        return Redirect::route('exams.show', $exam)->with('success', 'Exam Published.');
    }

    public function destroy($id){
        $exam = Exams::find($id);
        $exam->delete();
        return Redirect::route('exams')->with('success', 'Exam Deleted.');
    }

}
