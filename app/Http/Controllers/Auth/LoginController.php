<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Inertia\Inertia;
use App\UserSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function showLoginForm()
    {
        //return redirect()->away(env('APP_MAIN_URL') .'/login');
        return Inertia::render('Auth/Login');
    }

    public function AdminLoginForm()
    {
        return Inertia::render('Auth/Login');
    }

    public function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password','role');
        return $credentials;
    } 

    public function LoginWithWP(Request $request,$token){
        try {
            $decodeToken = str_replace('^mjn','D',$token);
            $user = UserSession::where('_token',$decodeToken)->first();
            $user = User::where('email',$user->email)->first();
            Auth::login($user, true);
            return Redirect::route('dashboard');
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return $this->sendResponse($errorInfo, 'Error!');
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        return response('', SymfonyResponse::HTTP_CONFLICT)->header('x-inertia-location', env('APP_MAIN_URL') . '/wp-login.php?action=logout');
        //return Redirect::route('login');
    }

    public function resetpassword() {
        Auth::logout();
        return response('', SymfonyResponse::HTTP_CONFLICT)->header('x-inertia-location', env('APP_MAIN_URL') . '/wp-login.php?action=logout&redirect_to=/forgot-password');
    }
}
