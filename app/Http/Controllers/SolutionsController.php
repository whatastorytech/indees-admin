<?php

namespace App\Http\Controllers;

use Request;

use App\Solution;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class SolutionsController extends Controller
{

    public function store(\Illuminate\Http\Request $request){   
        
        $url = $request->url;
        $urlCheck = $request->isTPurl;

        if(!$urlCheck){
            $url = $request->url;
        }else if( $request->file !== "null" && $request->file !== "undefined" ){
            $file = $request->file;
            $filename = $file->getClientOriginalName();
            $path = storage_path('app/public').'/solutions/';
            $url = $file->move($path, $filename);
        }
        
        $solution = Solution::where('question_id', '=',$request->question_id)->first();

        $data = [
            'explaination' => $request->explaination,
            'question_id' => $request->question_id,
            'video_url' => $url
        ];

        if ($solution === null) {
            Solution::create($data);
        } else{
            $solution->update($data);
        }
        
        return Redirect::to('question-bank/'.$request->question_id.'/show')->with('success', 'Solution Added.');
    }
    
}
