<?php

namespace App\Http\Controllers;

use App\Topics;
use App\Packages;
use App\Purchases;
use App\Subjects;
use App\Exams;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class PurchasesController extends Controller
{

    public function index(){
        //dd(Auth::user()->username);
        return Inertia::render('Purchases/Index', [
            'purchases' => Purchases::all()->where('username', '==', Auth::user()->username)
            ->transform(function ($data) {
                return [
                    'id' => $data->id,
                    'purchased_on' => $data->created_at->format('d, M, Y'),
                    'package_id' => $data->package_id
                ];
            }),
            'packages' => Packages::all()
        ]);
    }

}
