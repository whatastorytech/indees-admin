<?php

namespace App\Http\Controllers;

use App\User;
use App\Purchases;
use Inertia\Inertia;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ProfileController extends Controller
{
    public function show()
    {
        $user = Auth::user();

        if($user->role == 'admin'){
            return Inertia::render('Users/Profile', [
                'user' => [
                    'id' => $user->id,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                    'phone' => $user->phonenumber,
                    'address' => $user->address,
                    'city' => $user->city,
                    'state' => $user->state,
                    'district' => $user->district,
                    'pincode' => $user->postcode,
                    'avatar' => $user->avatar
                ]
            ]);
        }
        else{
            return Inertia::render('Users/Student/Profile', [
                'user' => [
                    'id' => $user->id,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                    'phone' => $user->phonenumber,
                    'address' => $user->address,
                    'city' => $user->city,
                    'state' => $user->state,
                    'district' => $user->district,
                    'pincode' => $user->postcode,
                    'avatar' => $user->avatar
                ],
                'purchases' => Purchases::all()->where('username', '==', Auth::user()->username)
                ->transform(function ($data) {
                    return [
                        'id' => $data->id,
                        'purchased_on' => $data->created_at->format('d, M, Y'),
                        'package_id' => $data->package_id
                    ];
                })
            ]);
        }
    }

    public function userSupport()
    {
        return Inertia::render('Users/Student/Support');
    }

    public function updateAvatar(Request $request){
        
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();

        $oldImg = public_path().'/avatars/'.$user->avatar;
        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();
        $request->avatar->move(public_path('/avatars'), $avatarName);
        $user->avatar = $avatarName;
        $saved = $user->save();
        if($saved){
            \File::delete($oldImg);
        }
        return back()->with('success');
    }

    public function updateProfile(Request $request){
        
        $request->validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'phone' => ['required'],
            'address' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'district' => ['required'],
            'pincode' => ['required']
        ]);

        $user = Auth::user();

        $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phonenumber' => $request->phone,
            'address' => $request->address,
            'state' => $request->state,
            'city' => $request->city,
            'district' => $request->district,
            'postcode' => $request->pincode
        ]);

        return back()->with('success');
    }
}
