<?php

namespace App\Http\Controllers;

use App\Result;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class ResultController extends Controller
{

    public function index(){
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exam_id = $request->exam_id;
        $username = $request->username;
        $user = \App\User::where('username','=',$username)->get()->first();
        $result = Result::create([
            'exam_id' => $exam_id,
            'username' => $username,
            'data' => $request->exam_data,
            'basic_result' => $request->basic_data,
            'score' => $request->score,
            'district' => $user->district,
            'state' => $user->state
        ]);
        //dd(Request::all());
        //if(!$result->exit)
            //return Redirect::route('result.show',$result->id);
        
        return Redirect::route('exams')->with('success', 'Your Exam result will be avilable soon');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
        
        if(Auth::user()->username === $result->username || Auth::user()->role == 'admin'){
            $exam = \App\Exams::find($result->exam_id);

            $now = Carbon::now();
            $end_date = Carbon::parse($exam->expiry);

            if(!($now->gt($end_date))){
                return Redirect::route('reports')->with('success', 'This report is not yet Avilable');
            }
            
            $questions = \App\QuestionBank::with('solution')->whereIn('id',$exam->questions_ids)->get()->toArray();
            $hquestions = \App\QuestionBankHindi::whereIn('question_id',$exam->questions_ids)->get()->toArray();
            $examresult = $result->data;
            $basic = $result->basic_result;
            $attempts = Result::with('exam')->get()
                        ->where('exam_id','=',$result->exam_id)
                        ->where('username','=',$result->username)->toArray();
            return Inertia::render('Exams/Student/Result',[
                'exam' => $exam,
                'questions' => $questions,
                'hquestions' => $hquestions,
                'result' => $examresult,
                'basic' => $basic,
                'attempts' => $attempts,
                'rank' => $result->getRanking(),
                'total' => $result->getTotal()
            ]);
        } else{
            return Redirect::route('reports')->with('success', 'You are not authorized to view this report.');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        //
    }
}
