<?php

namespace App\Http\Controllers;

use App\Result;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class ReportsController extends Controller
{
    public function __invoke()
    {
        $role = Auth::user()->role;

        if($role === "admin"){
            $attempts = Result::filter(Request::only('search','date','from','to','package'))
            ->orderBy('created_at', 'desc')
            ->with('exam')
            ->paginate(100)
            ->transform(function ($data) {
                return [
                    'id' => $data->id,
                    'taken_on' => $data->created_at->format('d, M, Y'),
                    'score' => $data->score,
                    'report_id' => $data->id,
                    'exam_id' => $data->exam_id,
                    'username' => $data->username,
                    'exam_name' => $data->exam->name
                ];
            })->groupBy('exam_id')->toArray();
            $exams = \App\Exams::all();
            return Inertia::render('Reports/Index' ,[
                'filters' => Request::all('search','date','from','to','package'),
                'exams' => $exams,
                'results' => $attempts
            ]);
        }
        else
            $results = Result::orderBy('created_at', 'desc')->get()->where('username','=',Auth::user()->username)
            ->transform(function ($data) {
                return [
                    'id' => $data->id,
                    'taken_on' => $data->created_at->format('d, M, Y'),
                    'score' => $data->score,
                    'report_id' => $data->id,
                    'exam_id' => $data->exam_id,
                    'exam_name' => $data->exam->name,
                    'available' => Carbon::now('Asia/Kolkata')->gt(Carbon::parse($data->exam->expiry)),
                    'a_on' => Carbon::parse($data->exam->expiry)->addDay()->format('M d, Y')
                ];
            })->toArray();

            //dd($results);
            return Inertia::render('Reports/Student/Index',[
                'results' => $results
            ]);
    }

    public function show(Result $result)
    {
        //dd('test');
        if(Auth::user()->role == 'admin'){
            $exam = \App\Exams::find($result->exam_id);
            $questions = \App\QuestionBank::all()->whereIn('id',$exam->questions_ids)->toArray();
            $examresult = $result->data;
            $basic = $result->basic_result;
            $attempts = Result::all()->where('exam_id','=',$result->exam_id)->where('username','=',$result->username)->toArray();
            return Inertia::render('Reports/Student/Show',[
                'exam' => $exam,
                'questions' => $questions,
                'result' => $examresult, 
                'basic' => $basic,
                'attempts' => $attempts,
                'rank' => $result->getRanking(),
                'total' => $result->getTotal()
            ]);
        } else if(Auth::user()->username === $result->username ){
            $exam = \App\Exams::find($result->exam_id);
            $questions = \App\QuestionBank::all()->whereIn('id',$exam->questions_ids)->toArray();
            $examresult = $result->data;
            $basic = $result->basic_result;
            //$attempts = Result::all()->where('exam_id','=',$result->exam_id)->where('username','=',$result->username)->toArray();
            return Inertia::render('Reports/Student/Show',[
                'exam' => $exam,
                'questions' => $questions,
                'result' => $examresult, 
                'basic' => $basic,
                //'attempts' => $attempts,
                'rank' => $result->getRanking(),
                'total' => $result->getTotal()
            ]);
        }
        
    }
}
