<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseControllerAPI as BaseControllerAPI;

class UsersController extends BaseControllerAPI
{

    public function index(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  
        
        $users = User::all();
        return $this->sendResponse($users->toArray(), 'users retrieved successfully.');

    }

    public function store(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $validator = Validator::make($input, [
            //'first_name' => 'required',
            //'last_name' => 'required',
            'username' => 'required',
            'email' => 'required',
            //'phonenumber' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        try {
            $user = User::Create([
                'username' => $request->username,
                'email' => $request->email,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phonenumber' => $request->phonenumber,
                'password' => $request->password,
                'address' => $request->address,
                'city' => $request->city,
                'district' => $request->district,
                'state' => $request->state,
                'country' => $request->country,
                'postcode' => $request->postcode,
                'role' => 'student'
            ]);

            return $this->sendResponse($user->toArray(), 'user created successfully.');

        } catch (\Illuminate\Database\QueryException $exception) {

            $errorInfo = $exception->errorInfo;

            return $this->sendResponse($errorInfo, 'Error!.');
        }

    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $user = User::where('email', '=', $id)->first();

        if (is_null($user)) {
            return $this->sendError('user not found.');
        }

        $user->password = $input['password'];
        $user->save();

        return $this->sendResponse($user->toArray(), 'user updated successfully.');
    }


    public function destroy(Reuqest $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  
        
        $user = Users::where('wp_id', '=', $id)->first();

        if (is_null($user)) {
            return $this->sendError('user not found.');
        }

        $user->delete();

        return $this->sendResponse($id, 'user deleted successfully.');
    }
}
