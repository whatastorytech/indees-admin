<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Contracts\Encryption\DecryptException;

class BaseControllerAPI extends Controller{

    //Function for checking Custom Auth (Custom token, Key Dencrytpion/Encrytion) 
    public function checkAuth($request){

        if(!$request->has('_token')) return false;
        
        $_token = str_replace('^mjn','ey',$request->_token);

        try {
            if(Crypt::decrypt($_token) == env('API_AUTH_KEY'))
                return true;
            else      
                return false;
        } catch (DecryptException $e) {
            return false;
        }
    }

    //Custom Response Function used for all api requests
    public function sendResponse($result, $message){
        return response()->json([
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ], 200);
    }

    //Custom Error Response Function used for all api requests
    public function sendError($error, $errorMessages = [], $code = 404){
    	$response = [ 'success' => false, 'message' => $error ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

}