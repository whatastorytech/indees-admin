<?php

namespace App\Http\Controllers\Api;

use App\Purchases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseControllerAPI as BaseControllerAPI;

class PurchasesController extends BaseControllerAPI
{

    public function index(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $purchases = Purchases::all();
        return $this->sendResponse($purchases->toArray(), 'purchases retrieved successfully.');
    
    }

    public function store(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $validator = Validator::make($input, [
            'order_id' => 'required',
            'package_id' => 'required',
            'username' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $purchase = Purchases::updateOrCreate([
            'package_id' => $request->package_id
        ],[
            'order_id' => $request->order_id,
            'package_id' => $request->package_id,
            'username' => $request->username
        ]);

        return $this->sendResponse($purchase->toArray(), 'Purchase created successfully.');
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $purchase = Purchases::where('wp_id', '=', $id)->first();

        if (is_null($purchase)) {
            return $this->sendError('purchase not found.');
        }

        $purchase->name = $input['name'];
        $purchase->slug = $input['slug'];
        $purchase->price = $input['price'];
        $purchase->url = $input['url'];
        $purchase->img = $input['img'];
        $purchase->save();

        return $this->sendResponse($purchase->toArray(), 'purchase updated successfully.');
    }


    public function destroy(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $purchase = Purchases::where('wp_id', '=', $id)->first();

        if (is_null($purchase)) {
            return $this->sendError('purchase not found.');
        }

        $purchase->delete();

        return $this->sendResponse($id, 'purchase deleted successfully.');
    }
}