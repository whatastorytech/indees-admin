<?php

namespace App\Http\Controllers\Api;

use App\Topics;
use App\Subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseControllerAPI as BaseControllerAPI;

class TopicsController extends BaseControllerAPI
{

    public function index(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $topics = Topics::all();
        return $this->sendResponse($topics->toArray(), 'topics retrieved successfully.');
    }

    public function store(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'wp_id' => 'required',
            'slug' => 'required',
            'parent_id' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $subject = Subjects::where('wp_id', '=', $request->parent_id)->first();
        $topic = Topics::create([
            'wp_id' => $request->wp_id,
            'name' => $request->name,
            'slug' => $request->slug,
            'subject_id' => $subject->id
        ]);

        return $this->sendResponse($topic->toArray(), 'topic created successfully.');

    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $topic = Topics::where('wp_id', '=', $id)->first();
        if (is_null($topic)) {
            return $this->sendError('topic not found.');
        }

        $subject = Subjects::where('wp_id', '=', $request->parent_id)->first();

        $topic->name = $input['name'];
        $topic->slug = $input['slug'];
        $topic->subject_id = $subject->id;
        $topic->save();

        return $this->sendResponse($topic->toArray(), 'topic updated successfully.');
    }


    public function destroy(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  
        
        $topic = Topics::where('wp_id', '=', $id)->first();

        if (is_null($topic)) {
            return $this->sendError('topic not found.');
        }

        $topic->delete();

        return $this->sendResponse($id, 'topic deleted successfully.');
    }
}
