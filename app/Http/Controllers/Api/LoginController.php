<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\UserSession;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends BaseControllerAPI {

    public function login(Request $request){
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);    

        if(!$request->has('email') ) 
            return response()->json([ 'success' =>false, 'message'=> 'Invalid Parameters'], 400);

        $user = User::where('email',$request->email)->first();

        if(!$user ) 
            return response()->json([ 'success' =>false, 'message'=> 'Invalid User'], 401);

        $_token = str_replace('D','^mjn',$user->remember_token);

        try {
            $session = UserSession::updateOrCreate([
                'email' => $request->email
            ],[
                '_token' => $user->remember_token
            ]);

            if($session) 
                return response()->json(['success'=> 1,'message'=> 'Authenticated Successfully.','_token'=> $_token]);
            else 
                return response()->json(['success'=> false,'message'=> 'Invalid Auth'], 401);

        } catch (QueryException $exception) {
            return $this->sendResponse($exception->errorInfo, 'Error!.');
        }
        
    }
}
