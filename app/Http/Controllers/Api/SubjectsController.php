<?php

namespace App\Http\Controllers\Api;

use App\Subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseControllerAPI as BaseControllerAPI;

class SubjectsController extends BaseControllerAPI
{

    public function index(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  
        
        $subjects = Subjects::all();
        return $this->sendResponse($subjects->toArray(), 'subjects retrieved successfully.');

    }

    public function store(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'wp_id' => 'required',
            'slug' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $subject = Subjects::create([
            'wp_id' => $request->wp_id,
            'name' => $request->name,
            'slug' => $request->slug
        ]);

        return $this->sendResponse($subject->toArray(), 'Subject created successfully.');
    }

    public function show($id)
    {
        // $subject = Subjects::find($id);


        // if (is_null($posubjectst)) {
        //     return $this->sendError('subject not found.');
        // }


        // return $this->sendResponse($subject->toArray(), 'subject retrieved successfully.');
    }

    public function update(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $subject = Subjects::where('wp_id', '=', $id)->first();
        if (is_null($subject)) {
            return $this->sendError('subject not found.');
        }

        $subject->name = $input['name'];
        $subject->slug = $input['slug'];
        $subject->save();

        return $this->sendResponse($subject->toArray(), 'subject updated successfully.');
    }


    public function destroy(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $subject = Subjects::where('wp_id', '=', $id)->first();

        if (is_null($subject)) {
            return $this->sendError('subject not found.');
        }

        $subject->delete();

        return $this->sendResponse($id, 'subject deleted successfully.');
    }
}