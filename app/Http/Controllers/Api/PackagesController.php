<?php

namespace App\Http\Controllers\Api;

use App\Packages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseControllerAPI as BaseControllerAPI;

class PackagesController extends BaseControllerAPI
{

    public function index(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $packages = Packages::all();
        return $this->sendResponse($packages->toArray(), 'Packages Retrieved Successfully.');
    }

    public function store(Request $request)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();
        $validator = Validator::make($input, [
            'wp_id' => 'required',
            'name' => 'required',
            'slug' => 'required',
            'price' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $package = Packages::updateOrCreate([
            'wp_id' => $request->wp_id
        ],[
            'wp_id' => $request->wp_id,
            'name' => $request->name,
            'slug' => $request->slug,
            'price' => $request->price,
            'url' => $request->url,
            'img' => $request->img
        ]);

        return $this->sendResponse($package->toArray(), 'Package created successfully.');
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  

        $input = $request->all();

        $package = Packages::where('wp_id', '=', $id)->first();

        if (is_null($package)) {
            return $this->sendError('package not found.');
        }

        $package->name = $input['name'];
        $package->slug = $input['slug'];
        $package->price = $input['price'];
        $package->url = $input['url'];
        $package->img = $input['img'];
        $package->save();

        return $this->sendResponse($package->toArray(), 'package updated successfully.');
    }


    public function destroy(Request $request, $id)
    {
        if(!$this->checkAuth($request)) 
            return response()->json([ 'success' =>false, 'message' => 'Unauthenticated' ], 401);  
            
        $package = Packages::where('wp_id', '=', $id)->first();

        if (is_null($package)) {
            return $this->sendError('package not found.');
        }

        $package->delete();

        return $this->sendResponse($id, 'package deleted successfully.');
    }
}
