<?php

namespace App\Http\Controllers;

use App\Exams;
use App\User;
use App\Result;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class LeaderboardController extends Controller
{
    public function __invoke()
    {
        $user = Auth::user();

        if($user->role === "admin"){

            $results = Result::with('exam')->paginate(50)->sortByDesc('created_at')->groupBy('exam_id')->map(function ($group) {
                // $item = $group->groupBy('username')->map(function ($group) {
                //     $item = $group->first();
                //     $item->user_rank = $item->getRanking();
                //     $item->total_users = $item->getTotal();
                //     $item->top100 = $item->getTop100();
                //     $item->topD100 = $item->getTop100('district');
                //     $item->topS100 = $item->getTop100('state');
                //     return $item;
                // });
                $item = $group->first();
                $item->exam = $item->exam;
                $item->total =  $item->getTotal();
                $item->top100 = $item->getTop100(null,true);
                return $item;
            });

            //dd($results);

            return Inertia::render('Leaderboard/Index' ,[
                'results' => $results
            ]);
        }
        else{
            $results = Result::where('username','=',$user->username)->with('exam')->get()->sortByDesc('created_at')->groupBy('exam_id')->map(function ($group) {
                $item = $group->first();
                $item->user_rank = $item->getRanking();
                $item->total_users = $item->getTotal();
                $item->top100 = $item->getTop100();
                $item->topD100 = $item->getTop100('district');
                $item->topS100 = $item->getTop100('state');
                $item->available = Carbon::now('Asia/Kolkata')->gt(Carbon::parse($item->exam->expiry));
                $item->a_on = Carbon::parse($item->exam->expiry)->addDay()->format('M d, Y');
                return $item;
            });

            return Inertia::render('Leaderboard/Student/Index',[
                'results' => $results
            ]);
        }
    }

}
