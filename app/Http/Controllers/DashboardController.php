<?php

namespace App\Http\Controllers;

use App\User;
use App\Exams;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __invoke()
    {
        $user_role = Auth::user()->role;

        if($user_role === "admin"){
            return Inertia::render('Dashboard/Index',[
                'total_students' => User::where('role','=','student')->count(),
                'total_exams' => Exams::count()
            ]);
        }
        else{
            $package_wp_ids = \App\Purchases::all()->where('username','=',Auth::user()->username)->pluck('package_id')->toArray();
           
            $package_ids = \App\Packages::whereIn('wp_id', $package_wp_ids)->pluck('id')->toArray();
            
            $packages = \App\Packages::whereIn('wp_id', $package_wp_ids)->get()->toArray();

            $examstaken = \App\Result::where('username','=',Auth::user()->username)->with('exam')->take(6)->get()
            ->transform(function ($data) {
                return [
                    'id' => $data->id,
                    'name' => $data->exam->name
                ];
            });

            $exams = \App\Exams::all()->whereIn('package_id',$package_ids)->take(6)
            ->transform(function ($data) {
                return [
                    'id' => $data->id,
                    'name' => $data->name
                ];
            });
            return Inertia::render('Dashboard/Student/Index',[
                'exams' => $exams,
                'reports' => $examstaken
            ]);
        }
    }
}
