<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchDB extends Controller
{
    public function searchStudents( Request $request )
    {
        $data = User::where('first_name', 'LIKE', "%{$request->input('query')}%")
                ->orwhere('last_name', 'LIKE', "%{$request->input('query')}%")
                ->orwhere('username', 'LIKE', "%{$request->input('query')}%")
                ->limit(50)->get()
                ->transform(function ($data) {
                    return [
                        'id' => $data->id,
                        'name' => $data->first_name . ' ' .$data->last_name,
                        'city' => $data->city,
                        'district' => $data->district
                    ];
                });

        return response()->json($data);
    }
}
