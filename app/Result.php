<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use UsesUuid;
    
    protected $fillable = ['exam_id','username','data','basic_result','score','district','state'];

    protected $casts = [
        'data' => 'array',
        'basic_result' => 'array',
    ];

    public function scopeFilter($query, array $filters)
    {
        
        $query->when($filters['package'] ?? null, function ($query, $data) {
            $query->whereHas('exam',function($query) use($data){
                $query->where('package_id', 'like', '%'.$data.'%');
            });
        })->when($filters['search'] ?? null, function ($query, $search) {
            $query->whereHas('exam',function($query) use($search){
                $query->where('name', 'like', '%'.$search.'%');
            });
        })->when($filters['date'] ?? null, function ($query, $date) {
            $dates = explode(",", htmlspecialchars_decode($date));
            $query->where(function ($query) use ($dates) {
                $query->where('created_at', '>=',date($dates[0]));
                $query->where('created_at', '<=',date($dates[1]));
            });
        });
    }

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = json_encode($value);
    }

    public function getDataAttribute($details)
    {
        return json_decode($details, true);
    }

    public function setBasicResultAttribute($value)
    {
        $this->attributes['basic_result'] = json_encode($value);
    }

    public function getBasicResultAttribute($details)
    {
        return json_decode($details, true);
    }

    public function getRanking(){
        $collection = collect(Result::orderBy('score', 'DESC')->get());
        $examid     = $collection->where('username', $this->username)->where('id', $this->id)->pluck('exam_id')->first();
        $data       = $collection->where('exam_id', $examid)->groupBy('username')->keys()->toArray();
        $value = array_search($this->username,$data)+1;
        return $value;
    }

    public function getTotal(){
        $collection = collect(Result::orderBy('created_at', 'DESC')->get());
        $examid     = $collection->where('id', $this->id)->pluck('exam_id')->first();
        $value       = $collection->where('exam_id', $examid)->groupby('username')->count();
        return $value;
    }

    public function getTop100($filter = null,$admin = false){
        $collection = Result::orderBy('score', 'DESC')->with(['user' => function($q){
            $q->select(['first_name', 'last_name','username','avatar']);
        }])->where('exam_id','=',$this->exam_id);

        if($filter == 'district'){
            $data = $collection->where('district','=',$this->district)->get();
        } 
        else if($filter == 'state'){
            $data = $collection->where('state','=',$this->state)->get();
        }
        else{
            $data = $collection->get();
        }
            
        $data = $data->groupBy('username')->map(function ($group) use ($admin) {
            $item = $group->first();
            if($admin)
                $item->user = $item->user;
            return $item;
        })->take(100);

        return $data;
    }

    public function exam(){
        return $this->belongsTo('App\Exams','exam_id');
    }

    public function user(){
        return $this->belongsTo('App\User','username','username');
    }

}
