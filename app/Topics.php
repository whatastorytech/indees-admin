<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topics extends Model
{
    use UsesUuid;
    public $timestamps = false;
    protected $fillable = ['name','wp_id','subject_id','slug'];
}
