<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSession extends Model
{
    use UsesUuid;

    protected $fillable = ['email','_token'];

    public function user()
    {
        return $this->belongsTo('App\User','email');
    }
}
